/** Time user must hold an icon before they may drag it [ms] */
const startDragDebounceDuration = 500;
/** Time user must stop dragging before the order updates [ms] */
const dragDebounceDuration = 100;
/** Map of icon positions */
const nodeMap = [];
/** 1/2 icon size [px] */
const iconSize = 41;

/** <body> element */
let bodyElement;
/** Entity the user drags around */
let draggableClone;
/** Total number of icons displayed */
let numApps;
/** Timer object for dragging to begin */
let startDragDebounceTimer;
/** Timer object for icon position to update */
let dragDebounceTimer;
/** Mouse position map */
let cursorPos;
/** Current node being held */
let currentNode;
/** All icon-wrapper elements */
let nodes;

/**
 * Ancient-IE-proofs the determination of cursor position,
 * accepted answer to Stack Overflow question:
 * [source](https://stackoverflow.com/questions/7790725/javascript-track-mouse-position)
 * @param {MouseEvent} event
 */
function getMousePosition( event ) {

  let eventDoc;
  let doc;
  let body;

  event = event || window.event; 
  
  if ( event.pageX == null && event.clientX != null ) {
    eventDoc = ( event.target && event.target.ownerDocument ) || document;
    doc = eventDoc.documentElement;
    body = eventDoc.body;

    event.pageX = event.clientX +
      ( doc && doc.scrollLeft || body && body.scrollLeft || 0 ) -
      ( doc && doc.clientLeft || body && body.clientLeft || 0 );
    event.pageY = event.clientY +
      ( doc && doc.scrollTop  || body && body.scrollTop  || 0 ) -
      ( doc && doc.clientTop  || body && body.clientTop  || 0 );
  }

  cursorPos = {
    x: event.pageX,
    y: event.pageY
  };

}

/** Updates the ordinal and coordinates of the node map */
function updateNodePositions() {

  nodes.forEach(( node ) => {

    nodeMap[node.style.order] = {
      x: node.offsetLeft + iconSize,
      y: node.offsetTop + iconSize
    };

  });

}

/**
 * Clones a DOM element for dragging and repositioning
 * in relation to the mouse
 */
function cloneNode() {

  draggableClone = currentNode.cloneNode( true );
  draggableClone.setAttribute( 'class', `${ draggableClone.getAttribute( 'class' )} drag` );
  draggableClone.firstElementChild.removeChild(draggableClone.firstElementChild.lastElementChild);
  currentNode.setAttribute( 'class', `${ currentNode.getAttribute( 'class' )} fade` );

  // Update icon positions
  updateNodePositions();

  // Initialze position
  draggableClone.style.order = null;
  draggableClone.style.top = `${ cursorPos.y - iconSize }px`;
  draggableClone.style.left = `${ cursorPos.x - iconSize }px`;
  bodyElement.prepend( draggableClone );

  // Bind dragging
  document.addEventListener( 'mousemove', dragIcon);

  // Bind release
  draggableClone.addEventListener( 'mouseup', destroyClone );

  // Bind 
  bodyElement.addEventListener( 'mouseleave', destroyClone );

}

/**
 * Control dragging behavior
 * @param {MouseEvent} event 
 */
function dragIcon( event ) {

  getMousePosition( event );
  draggableClone.style.top = `${ cursorPos.y - iconSize }px`;
  draggableClone.style.left = `${ cursorPos.x - iconSize }px`;

  if ( dragDebounceTimer ) clearTimeout( dragDebounceTimer );

  dragDebounceTimer = setTimeout( moveIcon, dragDebounceDuration );

}

/** Clears the drag debounce timer object */
function clearstartDragDebounce() {

  currentNode.removeEventListener( 'mouseup', clearstartDragDebounce );
  currentNode.removeEventListener( 'mouseleave', clearstartDragDebounce );
  currentNode.removeEventListener( 'mousemove', getMousePosition );
  clearTimeout( startDragDebounceTimer );

}

/** Destroy the draggable clone element */
function destroyClone() {

  bodyElement.removeChild( draggableClone );
  bodyElement.removeEventListener( 'mouseleave', destroyClone );
  document.removeEventListener( 'mousemove', dragIcon );
  currentNode.setAttribute( 'class', currentNode.getAttribute( 'class' ).replace( ' fade', '' ));

}

/** Finds the closest node to the cursor */
function getClosestNode() {

  return nodeMap.reduce(( min, node, i ) => {

    const r = Math.sqrt( Math.pow( cursorPos.x - node.x, 2 ) + Math.pow( cursorPos.y - node.y, 2 ));
    if ( r < min.r ) {
      min = {
        index: i,
        r
      }
    }

    return min;

  }, {
    index: undefined,
    r: Infinity
  }).index;

}

/** Moves the icon to its new position */
function moveIcon() {

  /** New position of dragged icon */
  let newOrdinal = getClosestNode();
  /** Old position of dragged icon */
  const oldOrdinal = parseInt( currentNode.style.order );
  // if nothing would change, do nothing
  if ( newOrdinal === oldOrdinal) return;
  // is it before or after the nearest node
  if ( nodeMap[newOrdinal].x > cursorPos.x ) newOrdinal --;
  // if nothing would change, do nothing
  if ( newOrdinal === oldOrdinal) return;

  if ( newOrdinal > oldOrdinal ) nodes.forEach(( node, i ) => {

    // record original order
    oldNodePos = parseInt( node.style.order );

    // bump every node that will be displaced
    if ( oldNodePos > oldOrdinal && oldNodePos <= newOrdinal ) node.style.order --;

  });

  else nodes.forEach(( node, i ) => {

    // record original order
    oldNodePos = parseInt( node.style.order );

    // bump every node that will be displacedW
    if ( oldNodePos >= newOrdinal && oldNodePos < oldOrdinal ) node.style.order ++;

  });

  // apply new ordinal to the cloned node
  currentNode.style.order = newOrdinal;

}

document.addEventListener( 'DOMContentLoaded', () => {

  nodes = document.querySelectorAll( '.icon-wrapper' );
  numApps = nodes.length;
  bodyElement = document.getElementsByTagName( 'body' )[0];
  
  nodes.forEach(( node, i ) => {

    // Make them different colors for testing
    const grayScale = Math.floor( 255 * i / numApps );
    node.firstElementChild.firstElementChild.style.backgroundColor = `rgba(${ grayScale.toString()},${ (255 - grayScale ).toString()},${ grayScale.toString() },1)`;

    // initialize node properties map
    node.style.order = i;

    // Setup start of drag
    node.addEventListener( 'mousedown', ( event ) => {

      currentNode = node;
      getMousePosition( event );

      startDragDebounceTimer = setTimeout(() => {
        currentNode.removeEventListener( 'mouseup', clearstartDragDebounce );
        currentNode.removeEventListener( 'mouseleave', clearstartDragDebounce );
        currentNode.removeEventListener( 'mousemove', getMousePosition );
        cloneNode();
      }, startDragDebounceDuration );

      currentNode.addEventListener( 'mouseup', clearstartDragDebounce );
      currentNode.addEventListener( 'mouseleave', clearstartDragDebounce );
      currentNode.addEventListener( 'mousemove', getMousePosition );

    });
  
  });

});
