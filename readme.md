# Smartphone Homescreen Emulator

Simple responsive web page which displays some rounded square icons and
allows the user to rearrange them as one would on iOS.

[Link to Page](https://saveriodesign.gitlab.io/explore-learning-assessment/)

## How to use

Click and hold for a half second to start dragging an app.  
App positions update after 1/10th of a second of not moving the mouse.  
Let go to place icon.

## How it works

I went with plain ol' HTML/CSS/JS mostly for giggles. I use flexbox to arrange
the icons because (1) I like flexbox and (2) it's easy to reorder elements
without touching the DOM. Icon positions are mapped when the click and hold
completes and dragging begins. Once the mouse-movement debounce expires (or
the user lets the icon go) it determines the closest grid position courtesy
of our friend Pythagoras. Then it compares x-values to see if it goes before
or after whats already there. Finally it bumps orders up and down, and flexbox
takes care of the rest.

I tried some janky translate-transition stuff to get an animation going but it only
added stutter to the teleportation that exists without the attempt. I believe
flexbox is a bad route if you want to animate the transitions. If I was going to
execute on a quick and dirty animation of this I'd move to an absolute positioning
scheme which responds to window resizing. I kind of hate that route.

## Sources

All icons are actual apps, their logos shamelessly sourced from
[iOS Icon Gallery](https://www.iosicongallery.com/).

IE-proof cursor position handling from Stack Overflow courtesy of 
[T.J. Crowder](https://stackoverflow.com/questions/7790725/javascript-track-mouse-position).

CSS Prefixes for ancient browsers from [Autoprefixer](https://autoprefixer.github.io/)
before being passed through the minifier.
